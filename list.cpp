#include <iostream>
#include "list.h"

using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ){
		p=head->next;
		delete head;
		head=p;
	}
}

void List::headPush(int i)
{
	Node *tmp = new Node(i , head);
	if(isEmpty()){
		tail = tmp;
	}
	head = tmp;
}

void List::tailPush(int i){
	Node *tmp = new Node(i);
	if(isEmpty()){
		tail = tmp;
		head = tail;
	}else{
		tail->next = tmp;
		tail = tmp;
	}
}

void List::output(){
	Node *tmp = head;
	while(tmp != 0){
		cout << tmp->info << " ";
		tmp = tmp->next;	
	}
}

int List::headPop(){
	Node *tmp;
	int A;
	tmp = head;
	A = tmp->info;
	head = head->next;
	delete tmp;
	if(head -> next == 0){
		tail = head;
	}
	return A;
	
}

int List::tailPop(){
	Node *tmp;
	Node *tmp2;
	int A;
	A = tail -> info;
	for(tmp = head ; tmp != 0 ; tmp = tmp->next){
		if(tmp->next->next == 0){
			tmp2 = tail;
			tail = tmp;
			tmp->next = 0;
			delete tmp2;
			break;
		}
	}
	return A;
}

void List::deleteNode(int i){
	Node *tmp;
	Node *tmp2;
	if(i == head->info){
		tmp = head;
		head = head -> next;
		delete tmp;
	}else{
		for(tmp = head ; tmp->next != 0 ; tmp = tmp -> next){
			if(tmp->next->info == i){
				tmp2 = tmp->next->next;
				delete tmp->next;
				tmp -> next = tmp2;
			}
		}
	}
	
}

bool List::isInList(int A){
	Node *tmp = head;
	for(tmp = head ; tmp != 0 ; tmp = tmp -> next){
		if(tmp->info == A){
			return true;
		}
	}
	return false;
}